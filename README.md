To install python 2.7:
``` sudo apt-get install python2.7 ```

To get pip for python 2:
``` wget https://bootstrap.pypa.io/pip/2.7/get-pip.py && python2.7 get-pip.py ```

To install virtual environment for python2:
``` 
python2.7 -m pip install virtualenv
python2.7 -m virtualenv VEpy2
source VEpy2/bin/activate
```

The install specific package versions:
```
# 2011 Feb 28
python2.7 -m pip install scipy==0.9.0 
# 2010 Nov 1
python2.7 -m pip install wxPython==2.8.12.1
# 2011 Jul 24
python2.7 -m pip install numpy==1.6.1
# 2006 Dec 3
python2.7 -m pip install PIL==1.1.7
# 2011 Nov 2
python2.7 -m pip install pyserial==2.6
# https://pypi.org/project/VideoCapture/#history ????
python2.7 -m pip install VideoCapture==0.9.4
```
