import wx
import serial
import time
import string
import numpy as np
import Image
from VideoCapture import *
from threading import Thread

import matplotlib
matplotlib.use('WXAgg')
from matplotlib.figure import Figure
from matplotlib.backends.backend_wxagg import \
    FigureCanvasWxAgg as FigCanvas, \
    NavigationToolbar2WxAgg as NavigationToolbar
import pylab


global st
st = '05000'
global sp
sp = ' 100'
global portnumber
portnumber = 0
global interval
interval = 0.2
global info
info = []
global camera
camera = Device(devnum=1)
global img






class MotorControl(wx.Dialog):

    def __init__(self, parent, title):
        super(MotorControl, self).__init__(parent, title=title,size=(800, 700))
        
        global img
        
        self.working = False
        self.worker = None
        self.isopen = False
        self.up1 = 0
        self.up2 = 0
        self.up3 = 0
        self.up4 = 0
        global cmr
        cmr = 0
        self.data1 = [0]
        self.data2 = [0]
        
        self.InitUI()
        self.Centre()
        self.Show()

    def InitUI(self):

        self.init_plot()
        self.canvas = FigCanvas(self, wx.NewId(), self.fig)
        
        
        box = wx.BoxSizer(wx.HORIZONTAL)
        
        vbox = wx.StaticBoxSizer(wx.StaticBox(self,-1,'Configuration'),wx.VERTICAL)
        
        basics = wx.StaticBoxSizer(wx.StaticBox(self, -1, "Basics"), wx.VERTICAL)
        bbsp = wx.BoxSizer(wx.HORIZONTAL)
        self.speedt = wx.StaticText(self, -1, "Speed (mm/s)")
        self.speed = wx.TextCtrl(self, 1, "")
        bbsp.Add(self.speedt, 1, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 4)
        bbsp.Add(self.speed, 1, 0, 0)
        bbst = wx.BoxSizer(wx.HORIZONTAL)
        self.stept = wx.StaticText(self, -1, "Distance (mm)")
        self.step = wx.TextCtrl(self, 2, "")
        bbst.Add(self.stept, 1, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 4)
        bbst.Add(self.step, 1, 0, 0)
        basics.Add(bbsp, 0, wx.EXPAND, 0)
        basics.Add(bbst, 0, wx.EXPAND, 0)
        
        port = wx.StaticBoxSizer(wx.StaticBox(self, -1, "Port"), wx.VERTICAL)
        bpn = wx.BoxSizer(wx.HORIZONTAL)
        self.portt = wx.StaticText(self, -1, "Port")
        chc = ['COM 1','COM 2','COM 3','COM 4','COM 5']
        ID_COM = wx.NewId()
        self.combopn = wx.ComboBox(self, ID_COM, choices=chc, style=wx.CB_DROPDOWN)
        self.combopn.SetValue('COM 1')
        #self.combopn.Disable()
        
        self.Bind(wx.EVT_COMBOBOX, self.OnCbCom, id=ID_COM)
        
        bpn.Add(self.portt, 1, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 4)
        bpn.Add(self.combopn, 1, 0, 0)
        bpf = wx.BoxSizer(wx.HORIZONTAL)
        self.fre = wx.StaticText(self, -1, "Frequency")
        chc = ['1 Hz','2 Hz','3 Hz','4 Hz','5 Hz']
        ID_FR = wx.NewId()
        self.combopf = wx.ComboBox(self, ID_FR, choices=chc, style=wx.CB_DROPDOWN)
        self.combopf.SetValue('5 Hz')
        
        self.Bind(wx.EVT_COMBOBOX, self.OnCbFr, id=ID_FR)

        bpf.Add(self.fre, 1, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 4)
        bpf.Add(self.combopf, 1, 0, 0)
        port.Add(bpn, 0, wx.EXPAND, 0)
        port.Add(bpf, 0, wx.EXPAND, 0)
        
        cam = wx.StaticBoxSizer(wx.StaticBox(self, -1, "Camera"), wx.HORIZONTAL)
        self.device = wx.StaticText(self, -1, "Device Number")
        chc = ['one','two']
        ID_CAM = wx.NewId()
        self.cc = wx.ComboBox(self, ID_CAM, choices=chc, style=wx.CB_DROPDOWN)
        self.cc.SetValue('one')
        
        self.Bind(wx.EVT_COMBOBOX, self.OnCamera, id=ID_CAM)
        
        cam.Add(self.device, 1, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 4)
        cam.Add(self.cc, 1, 0, 0)

        Motor = wx.StaticBoxSizer(wx.StaticBox(self, -1, "Motor"), wx.VERTICAL)
        bm1 = wx.BoxSizer(wx.HORIZONTAL)
        self.m1 = wx.StaticText(self, -1, "Motor 1")
        chc = ['off','up','down']
        
        ID_M1 = wx.NewId()
        self.cm1 = wx.ComboBox(self, ID_M1, choices=chc, style=wx.CB_DROPDOWN)
        self.cm1.SetValue('off')
        
        self.Bind(wx.EVT_COMBOBOX, self.OnCbM1, id=ID_M1)
        
        bm1.Add(self.m1, 1, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 4)
        bm1.Add(self.cm1, 1, 0, 0)
        bm2 = wx.BoxSizer(wx.HORIZONTAL)
        self.m2 = wx.StaticText(self, -1, "Motor 2")
        
        ID_M2 = wx.NewId()
        self.cm2 = wx.ComboBox(self, ID_M2, choices=chc, style=wx.CB_DROPDOWN)
        self.cm2.SetValue('off')
        
        self.Bind(wx.EVT_COMBOBOX, self.OnCbM2, id=ID_M2)
        
        bm2.Add(self.m2, 1, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 4)
        bm2.Add(self.cm2, 1, 0, 0)
        bm3 = wx.BoxSizer(wx.HORIZONTAL)
        self.m3 = wx.StaticText(self, -1, "Motor 3")
        
        ID_M3 = wx.NewId()
        self.cm3 = wx.ComboBox(self, ID_M3, choices=chc, style=wx.CB_DROPDOWN)
        self.cm3.SetValue('off')
        
        self.Bind(wx.EVT_COMBOBOX, self.OnCbM3, id=ID_M3)
        
        bm3.Add(self.m3, 1, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 4)
        bm3.Add(self.cm3, 1, 0, 0)
        bm4 = wx.BoxSizer(wx.HORIZONTAL)
        self.m4 = wx.StaticText(self, -1, "Motor 4")
        
        ID_M4 = wx.NewId()
        self.cm4 = wx.ComboBox(self, ID_M4, choices=chc, style=wx.CB_DROPDOWN)
        self.cm4.SetValue('off')
        
        self.Bind(wx.EVT_COMBOBOX, self.OnCbM4, id=ID_M4)
        
        bm4.Add(self.m4, 1, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 4)
        bm4.Add(self.cm4, 1, 0, 0)
        
        Motor.Add(bm1, 0, wx.EXPAND, 0)
        Motor.Add(bm2, 0, wx.EXPAND, 0)
        Motor.Add(bm3, 0, wx.EXPAND, 0)
        Motor.Add(bm4, 0, wx.EXPAND, 0)
        
        btn = wx.BoxSizer(wx.HORIZONTAL)
        ID_START = wx.NewId()
        self.start = wx.Button(self, ID_START,label='Start')
        
        self.Bind(wx.EVT_BUTTON, self.on_start_button, id=ID_START)
        
        btn.Add(self.start)
    
        vbox.Add(basics, 0, wx.EXPAND, 0)
        vbox.Add(port, 0, wx.EXPAND, 0)
        vbox.Add(cam, 0, wx.EXPAND, 0)
        vbox.Add(Motor, 0, wx.EXPAND, 0)
        vbox.Add(btn, flag=wx.ALIGN_RIGHT|wx.RIGHT|wx.TOP, border=1)
        
        movie = wx.StaticBoxSizer(wx.StaticBox(self, -1, "Camera"), wx.VERTICAL)
        
        self.panel = wx.Panel(self, wx.NewId(), size=(544,295 ))
        
        movie.Add(self.panel,0, wx.EXPAND, 0)
        
        box.Add(vbox,flag=wx.ALL, border=5)
        box.Add(movie,0, wx.GROW|wx.ALIGN_CENTER_HORIZONTAL|wx.ALL, 5)
    
        

        sizer = wx.BoxSizer(wx.VERTICAL)
        sizer.Add(box, 0, wx.EXPAND,0)
        
        drawbox = wx.StaticBoxSizer(wx.StaticBox(self, -1, "Graph"),wx.VERTICAL)
        drawbox.Add(self.canvas, 1, wx.GROW|wx.ALIGN_CENTER_HORIZONTAL|wx.ALL,0)
        
        sizer.Add(drawbox, 0, wx.EXPAND|wx.ALL,5)
            
        EVT_RESULT(self,self.OnResult)        
        self.SetSizer(sizer)
    
    def init_plot(self):
        
        self.dpi = 100
        self.fig = Figure((8.0, 3.0), dpi=self.dpi)
    
        self.axes1 = self.fig.add_subplot(121)
        self.axes1.set_axis_bgcolor('black')
        self.axes1.set_title('Load Cell 1', size=12)
        
        self.axes2 = self.fig.add_subplot(122)
        self.axes2.set_axis_bgcolor('black')
        self.axes2.set_title('Load cell 2', size=12)
        
        pylab.setp(self.axes1.get_xticklabels(), fontsize=8)
        pylab.setp(self.axes1.get_yticklabels(), fontsize=8)
        
        pylab.setp(self.axes2.get_xticklabels(), fontsize=8)
        pylab.setp(self.axes2.get_yticklabels(), fontsize=8)

        self.plot_data1 = self.axes1.plot(
            self.data1, 
            linewidth=1,
            color=(1, 1, 0),
            )[0]
            
        self.plot_data2 = self.axes2.plot(
            self.data2, 
            linewidth=1,
            color=(1, 1, 0),
            )[0]
        
        #self.draw()
           
    def on_start_button(self,event):
        #
        if not self.worker:
            
            dlg = wx.DirDialog(self,"Choose a directory:", style=wx.DD_DEFAULT_STYLE | wx.DD_NEW_DIR_BUTTON)
            if dlg.ShowModal() == wx.ID_OK:  
                #self.SetStatusText('You selected: %s\n' % dlg.GetPath())
                #f = open(dlg.GetPath()+'\\khar.txt')
                path = dlg.GetPath()
                pass  
            else:
                self.Result(event)
                return     
            dlg.Destroy()
            
            sp1 = self.speed.GetValue()
            spd = float(sp1)
            st1 = self.step.GetValue()
            dur = float(st1)/spd
            sp1 = int(1/(0.1013245*float(sp1)))
            st1 = int(float(st1)*1000/1.51)
            
            st = string.zfill(str(st1), 5)
            sp = ' '+str(sp1)
            info.append(path)
            info.append(self.up1)
            info.append(self.up2)
            info.append(self.up3)
            info.append(self.up4)
            info.append(cmr)
            info.append(st)
            info.append(sp)
            #print info[6]
            #print info[7]

            self.worker = WorkerThread(self,self.up1,self.up2,self.up3,self.up4,st,sp,spd,dur,path)

            self.Disable()
        else:
            self.worker.abort()
            self.Result()
        
    def OnResult(self, event):
        if event.data==0:
            self.Result()
        else:
            #self.paint()
            self.draw()
            
    
    def Result(self):
        self.Enable()
        self.worker = None
        
    def paint(self):
        image = self.pil_to_image(self.worker.img)
        bmp = wx.BitmapFromImage(image)
        dc = wx.PaintDC(self.panel)
        dc.DrawBitmap(bmp, 0, 0, True)
        
    def draw(self):
        
        self.data1.append(self.worker.L1)
        self.data2.append(self.worker.L2)

        xmax = len(self.data1) if len(self.data1) > 50 else 50
        #xmin = xmax - 50
        xmin = 0

        ymin1 = round(min(self.data1), 0) - 1 
        ymin2 = round(min(self.data2), 0) - 1 
        
        ymax1 = round(max(self.data1), 0) + 1 
        ymax2 = round(max(self.data2), 0) + 1 
        
        self.axes1.set_xbound(lower=xmin, upper=xmax)
        self.axes1.set_ybound(lower=ymin1, upper=ymax1) 
        
        self.axes2.set_xbound(lower=xmin, upper=xmax)
        self.axes2.set_ybound(lower=ymin2, upper=ymax2)
        
        self.axes1.grid(True, color='gray')
        self.axes2.grid(True, color='gray')
    
        self.plot_data1.set_xdata(np.arange(len(self.data1)))
        self.plot_data1.set_ydata(np.array(self.data1))
        self.plot_data2.set_xdata(np.arange(len(self.data2)))
        self.plot_data2.set_ydata(np.array(self.data2))

        self.canvas.draw()
    
    def OnCamera(self, event):
        c = self.cc.GetValue()
        if c=='one':
            cmr = 0
        else:
            cmr = 1

        
    def OnCbCom(self, event):
        p = self.combopn.GetValue()
        p = p.split(' ')
        portnumber = int(p[1])
        
    
    def OnCbFr(self, event):
        f = self.combopf.GetValue()
        f = f.split(' ')
        fr = float(f[0])
        interval = 1/fr

    
    def OnCbM1(self, event):
        M1 = self.cm1.GetValue()
        if M1=='up':
            self.up1 = 1
        elif M1=='down':
            self.up1 = 2
    
    def OnCbM2(self, event):
        M2 = self.cm2.GetValue()
        if M2=='up':
            self.up2 = 1
        elif M2=='down':
            self.up2 = 2

    
    def OnCbM3(self, event):
        M3 = self.cm3.GetValue()
        if M3=='up':
            self.up3 = 1
        elif M3=='down':
            self.up3 = 2

    
    def OnCbM4(self, event):
        M4 = self.cm4.GetValue()
        if M4=='up':
            self.up4 = 1
        elif M4=='down':
            self.up4 = 2
    
    def Disable(self):
    
            self.speed.Disable()
            self.step.Disable()
            self.combopf.Disable()
            self.combopn.Disable()
            self.cc.Disable()
            self.cm1.Disable()
            self.cm2.Disable()
            self.cm3.Disable()
            self.cm4.Disable()
            self.start.SetLabel('Stop') 
    
    def Enable(self):
            
            self.speed.Enable()
            self.step.Enable()
            self.combopf.Enable()
            self.combopn.Enable()
            self.cc.Enable()
            self.cm1.Enable()
            self.cm2.Enable()
            self.cm3.Enable()
            self.cm4.Enable()
            self.start.SetLabel('Start') 

    def pil_to_image(self, pil, alpha=True):
        if alpha:
            image = apply( wx.EmptyImage, pil.size )
            image.SetData( pil.convert( "RGB").tostring() )
            image.SetAlphaData(pil.convert("RGBA").tostring()[3::4])
        else:
            image = wx.EmptyImage(pil.size[0], pil.size[1])
            new_image = pil.convert('RGB')
            data = new_image.tostring()
            image.SetData(data)
        return image


    def image_to_pil(self, image):
        pil = Image.new('RGB', (image.GetWidth(), image.GetHeight()))
        pil.fromstring(image.GetData())
        return pil


EVT_RESULT_ID = wx.NewId()

def EVT_RESULT(win, func):
    """Define Result Event."""
    win.Connect(-1, -1, EVT_RESULT_ID, func)

class ResultEvent(wx.PyEvent):
    """Simple event to carry arbitrary result data."""
    def __init__(self,data):
        """Init Result Event."""
        wx.PyEvent.__init__(self)
        self.SetEventType(EVT_RESULT_ID)
        self.data = data
        
class WorkerThread(Thread):
    """Worker Thread Class."""
    def __init__(self, notify_window, up1, up2, up3, up4, st, sp, spd, dur,dlg):
        """Init Worker Thread Class."""
        Thread.__init__(self)
        self._notify_window = notify_window
        self._want_abort = 0

        self.L2 = 0
        self.L1 = 0
        global img
        global cmr
        self.pic = 0
        self.ser = 0
        self.camera = 0
        self.data = info
        self.L1_off = np.zeros(20)
        self.L2_off = np.zeros(20)
        self.L1off = 0
        self.L2off = 0
        self.L1_SLope = 0.047
        self.L2_SLope = 0.062
        self.img = 0
        self.dur = dur
        
        self.up1 = up1
        self.up2 = up2
        self.up3 = up3
        self.up4 = up4
        self.st = st
        self.sp = sp
        self.spd = spd
        self.dlg = dlg
        
        # This starts the thread running on creation, but you could
        # also make the GUI thread responsible for calling this
        self.start()

    def run(self):
        """Run Worker Thread."""
        

        try:
            self.ser = serial.Serial('COM1',
                                baudrate=38400, 
                                bytesize=8, 
                                parity='N', 
                                stopbits=1, 
                                writeTimeout=0.200,
                                timeout = 0.300,
                                interCharTimeout=None)
            self.ser.setDTR(level=False)
            self.ser.setRTS(level=False)
            self.ser.timeout = 0.300
        except:
            wx.MessageBox('A problem in reading from port', 'error', wx.OK | wx.ICON_ERROR)
            wx.PostEvent(self._notify_window, ResultEvent(0))
            return

        try:
            camera = Device(devnum=1)
        except:
            wx.MessageBox('A problem in reading from camera', 'error', wx.OK | wx.ICON_ERROR)
            wx.PostEvent(self._notify_window, ResultEvent(0))
            return
        #calibration
        for i in range(20):
            self.ser.writelines('L1'+'\r')
            a1 = self.ser.readline()
            a1 = float(a1)
            self.L1_off[i] = a1
            self.ser.writelines('L2'+'\r')
            a2 = self.ser.readline()
            a2 = float(a2)
            self.L2_off[i] = a2
        
        self.L1off = np.average(self.L1_off)
        self.L2off = np.average(self.L2_off)
        #calibration

        
        i = 0
        
        if self.up1+self.up2+self.up3+self.up4>0:
            f = open(self.dlg+'\\data.txt','w')
            self.ser.writelines('S'+self.sp+'\r')       
            if self.up1==1:
                char = 'M1U'+self.st+'\r'
                self.ser.writelines(char)
            elif self.up1==2:
                char = 'M1D'+self.st+'\r'
                self.ser.writelines(char)

            if self.up2==1:
                char = 'M2U'+self.st+'\r'
                self.ser.writelines(char)
            elif self.up2==2:
                char = 'M2D'+self.st+'\r'
                self.ser.writelines(char)
                            
            if self.up3==1:
                char = 'M3U'+self.st+'\r'
                self.ser.writelines(char)
            elif self.up3==2:
                char = 'M3D'+self.st+'\r'
                self.ser.writelines(char)
                            
            if self.up4==1:
                char = 'M4U'+self.st+'\r'
                self.ser.writelines(char)
            elif self.up4==2:
                char = 'M4D'+self.st+'\r'
                self.ser.write(char)
            
            start = time.time()
            t = 0
            while t<self.dur:
                                
                    char = 'L1'+'\r'
                    self.ser.writelines(char)
                    a1 = self.ser.readline()
                    start1 = time.time()-start
                    a1 = float(a1)
                        
                    char = 'L2'+'\r'
                    self.ser.writelines(char)
                    a2 = self.ser.readline()
                    start2 = time.time()-start
                    a2 = float(a2)
                    
                    
                    self.L1 = ((a1-self.L1off)*9.8*self.L1_SLope/1000)
                    self.L2 = ((a2-self.L2off)*9.8*self.L2_SLope/1000)

                    
                    if self.L1>14.7 or self.L2>14.7:
                        wx.PostEvent(self._notify_window, ResultEvent(0))
                        self.abort()
                        return
                    
                    f.writelines([str(self.spd*2*start1),'\t',str(self.L1),'\t\t',str(self.spd*2*start2),'\t',str(self.L2)
                                  ,'\t\t',string.zfill(str(i), 5),'\r\n'])
                                        
                    #camera.saveSnapshot(self.data[0]+'\\'+string.zfill(str(i), 5)+'.jpg')
                    self.img = camera.getImage()
                    self.img.save(self.dlg+'\\'+string.zfill(str(i), 5)+'.jpg')
                    
                    wx.PostEvent(self._notify_window, ResultEvent(1))
                    
                    i += 1    
                    time.sleep(interval)
                    if self._want_abort:
                        wx.PostEvent(self._notify_window, ResultEvent(0))
                        return
                    t = time.time()-start
        wx.PostEvent(self._notify_window, ResultEvent(0))
    wx.EVT_HOTKEY
    def abort(self):
        if self.ser._isOpen:
            self.ser.writelines('P1'+'\r')
            self.ser.writelines('P2'+'\r')
            self.ser.writelines('P3'+'\r')
            self.ser.writelines('P4'+'\r')
            self.ser.close()
        self._want_abort = 1
        

if __name__ == '__main__':
  
    app = wx.App()
    MotorControl(None, title="Motor Control")
    app.MainLoop()