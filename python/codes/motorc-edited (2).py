import wx
import serial
import time
import string
import numpy as np
import Image
from VideoCapture import *
from threading import Thread
from scipy.optimize import fmin_cobyla
from op1 import opt  #change

import matplotlib
matplotlib.use('WXAgg')
from matplotlib.figure import Figure
from matplotlib.backends.backend_wxagg import \
    FigureCanvasWxAgg as FigCanvas, \
    NavigationToolbar2WxAgg as NavigationToolbar
import matplotlib.pyplot as plt


global st
st = '05000'
global sp
sp = ' 100'
global portnumber
portnumber = 0
global interval
interval = 0.2
global info
info = []
global camera
camera = Device(devnum=1)   #change
#camera = 3
global img

    
class MotorControl(wx.Frame):

    def __init__(self, parent, title):
        super(MotorControl, self).__init__(parent, title=title,size=(810, 720))
        #wx.Frame.__init__(self, parent, -1, "Motor Control",size=[800,700])
        self.SetBackgroundColour([255,255,255])
        self.working = False
        self.worker = None
        self.isopen = False
        
        menubar = wx.MenuBar()
        fileMenu = wx.Menu()
        
        anlitm = fileMenu.Append(wx.NewId(), 'Optimize')
        fileMenu.AppendSeparator()
        quit = fileMenu.Append(wx.ID_EXIT, 'Quit', 'Quit application')
        menubar.Append(fileMenu, '&File')
        
        
        self.Bind(wx.EVT_MENU, self.OnOpt, anlitm)
        self.Bind(wx.EVT_MENU, self.OnQuit, quit)
        
        self.SetMenuBar(menubar)
        self.up1 = 0
        self.up2 = 0
        self.up3 = 0
        self.up4 = 0
        global cmr
        cmr = 0
        self.data1 = [0]
        self.data2 = [0]
        
        self.InitUI()
        self.Centre()
        self.Show()

    def InitUI(self):

        self.init_plot()
        self.canvas = FigCanvas(self, wx.NewId(), self.fig)
        
        box = wx.BoxSizer(wx.HORIZONTAL)
        
        vbox = wx.StaticBoxSizer(wx.StaticBox(self,-1,'Configuration'),wx.VERTICAL)
        
        basics = wx.StaticBoxSizer(wx.StaticBox(self, -1, "Basics"), wx.VERTICAL)
        
        bpl = wx.BoxSizer(wx.HORIZONTAL)
        self.typeofl = wx.StaticText(self, -1, "Type of Loading")
        chc = ['Static','Cyclic']
        ID_LOAD = wx.NewId()
        self.comboLO = wx.ComboBox(self, ID_LOAD, choices=chc, style=wx.CB_DROPDOWN)
        self.comboLO.SetValue('Static')
        #self.combopn.Disable()
        
        self.Bind(wx.EVT_COMBOBOX, self.OncomboLO, id=ID_LOAD)
        
        
        bpl.Add(self.typeofl, 1, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 4)
        bpl.Add(self.comboLO, 1, 0, 0)

        bbsp = wx.BoxSizer(wx.HORIZONTAL)
        self.speedt = wx.StaticText(self, -1, "Speed (mm/s)")
        self.speed = wx.TextCtrl(self, 1, "")
        
        bpf = wx.BoxSizer(wx.HORIZONTAL)
        self.fre = wx.StaticText(self, -1, "Frequency (Hz)")
        self.fret = wx.TextCtrl(self, 1, "")
        bpf.Add(self.fre, 1, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 4)
        bpf.Add(self.fret, 1, 0, 0)


        bpc = wx.BoxSizer(wx.HORIZONTAL)
        self.ncyclestatic = wx.StaticText(self, -1, "Number of cycles")
        self.ncycle = wx.TextCtrl(self, 1, "")
        bpc.Add(self.ncyclestatic, 1, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 4)
        bpc.Add(self.ncycle, 1, 0, 0)

        
        bbsp.Add(self.speedt, 1, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 4)
        bbsp.Add(self.speed, 1, 0, 0)
        bbst = wx.BoxSizer(wx.HORIZONTAL)
        self.stept = wx.StaticText(self, -1, "Distance (mm)")
        self.step = wx.TextCtrl(self, 2, "")
        bbst.Add(self.stept, 1, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 4)
        bbst.Add(self.step, 1, 0, 0)
        check = wx.BoxSizer(wx.HORIZONTAL)
        self.cb = wx.CheckBox(self, -1, "Preloading (N)")
        self.cb.SetValue(True)
        
        self.cr = wx.CheckBox(self, -1, "Relaxation")
        self.cr.SetValue(False)
        
        self.cbctrl = wx.TextCtrl(self,3,"")
        check.Add(self.cb,1,wx.ALL|wx.ALIGN_CENTER_VERTICAL, 4)
        check.Add(self.cbctrl,1,0, 0)
        basics.Add(bpl, 0, wx.EXPAND, 0)
        basics.Add(bbsp, 0, wx.EXPAND, 0)
        basics.Add(bpf, 0, wx.EXPAND, 0)
        basics.Add(bpc, 0, wx.EXPAND, 0)
        basics.Add(bbst, 0, wx.EXPAND, 0)
        basics.Add(check,0,wx.EXPAND, 0)
        basics.Add(self.cr,0,wx.ALL|wx.ALIGN_CENTER_VERTICAL, 4)
        self.Bind(wx.EVT_CHECKBOX, self.onCheck, self.cb)
        
        
        port = wx.StaticBoxSizer(wx.StaticBox(self, -1, "Port"), wx.VERTICAL)
        bpn = wx.BoxSizer(wx.HORIZONTAL)
        self.portt = wx.StaticText(self, -1, "Port")
        chc = ['COM 1','COM 2','COM 3','COM 4','COM 5']
        ID_COM = wx.NewId()
        self.combopn = wx.ComboBox(self, ID_COM, choices=chc, style=wx.CB_DROPDOWN)
        self.combopn.SetValue('COM 1')
        #self.combopn.Disable()
        
        self.Bind(wx.EVT_COMBOBOX, self.OnCbCom, id=ID_COM)
        
        bpn.Add(self.portt, 1, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 4)
        bpn.Add(self.combopn, 1, 0, 0)
        #bpf = wx.BoxSizer(wx.HORIZONTAL)
        #self.fre = wx.StaticText(self, -1, "Frequency")
        #chc = ['1 Hz','2 Hz','3 Hz','4 Hz','5 Hz']
        #ID_FR = wx.NewId()
        #self.combopf = wx.ComboBox(self, ID_FR, choices=chc, style=wx.CB_DROPDOWN)
        #self.combopf.SetValue('5 Hz')
        
        #self.Bind(wx.EVT_COMBOBOX, self.OnCbFr, id=ID_FR)

        #bpf.Add(self.fre, 1, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 4)
        #bpf.Add(self.combopf, 1, 0, 0)
        port.Add(bpn, 0, wx.EXPAND, 0)
        #port.Add(bpf, 0, wx.EXPAND, 0)
        
        cam = wx.StaticBoxSizer(wx.StaticBox(self, -1, "Camera"), wx.HORIZONTAL)
        self.device = wx.StaticText(self, -1, "Device Number")
        chc = ['one','two']
        ID_CAM = wx.NewId()
        self.cc = wx.ComboBox(self, ID_CAM, choices=chc, style=wx.CB_DROPDOWN)
        self.cc.SetValue('one')
        
        self.Bind(wx.EVT_COMBOBOX, self.OnCamera, id=ID_CAM)
        
        cam.Add(self.device, 1, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 4)
        cam.Add(self.cc, 1, 0, 0)

        Motor = wx.StaticBoxSizer(wx.StaticBox(self, -1, "Motor"), wx.VERTICAL)
        bm1 = wx.BoxSizer(wx.HORIZONTAL)
        self.m1 = wx.StaticText(self, -1, "Motor 1")
        chc = ['off','up','down']
        
        ID_M1 = wx.NewId()
        self.cm1 = wx.ComboBox(self, ID_M1, choices=chc, style=wx.CB_DROPDOWN)
        self.cm1.SetValue('off')
        
        self.Bind(wx.EVT_COMBOBOX, self.OnCbM1, id=ID_M1)
        
        bm1.Add(self.m1, 1, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 4)
        bm1.Add(self.cm1, 1, 0, 0)
        bm2 = wx.BoxSizer(wx.HORIZONTAL)
        self.m2 = wx.StaticText(self, -1, "Motor 2")
        
        ID_M2 = wx.NewId()
        self.cm2 = wx.ComboBox(self, ID_M2, choices=chc, style=wx.CB_DROPDOWN)
        self.cm2.SetValue('off')
        
        self.Bind(wx.EVT_COMBOBOX, self.OnCbM2, id=ID_M2)
        
        bm2.Add(self.m2, 1, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 4)
        bm2.Add(self.cm2, 1, 0, 0)
        bm3 = wx.BoxSizer(wx.HORIZONTAL)
        self.m3 = wx.StaticText(self, -1, "Motor 3")
        
        ID_M3 = wx.NewId()
        self.cm3 = wx.ComboBox(self, ID_M3, choices=chc, style=wx.CB_DROPDOWN)
        self.cm3.SetValue('off')
        
        self.Bind(wx.EVT_COMBOBOX, self.OnCbM3, id=ID_M3)
        
        bm3.Add(self.m3, 1, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 4)
        bm3.Add(self.cm3, 1, 0, 0)
        bm4 = wx.BoxSizer(wx.HORIZONTAL)
        self.m4 = wx.StaticText(self, -1, "Motor 4")
        
        ID_M4 = wx.NewId()
        self.cm4 = wx.ComboBox(self, ID_M4, choices=chc, style=wx.CB_DROPDOWN)
        self.cm4.SetValue('off')
        
        self.Bind(wx.EVT_COMBOBOX, self.OnCbM4, id=ID_M4)
        
        bm4.Add(self.m4, 1, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 4)
        bm4.Add(self.cm4, 1, 0, 0)
        
        Motor.Add(bm1, 0, wx.EXPAND, 0)
        Motor.Add(bm2, 0, wx.EXPAND, 0)
        Motor.Add(bm3, 0, wx.EXPAND, 0)
        Motor.Add(bm4, 0, wx.EXPAND, 0)
        
        btn = wx.BoxSizer(wx.HORIZONTAL)
        ID_START = wx.NewId()
        self.start = wx.Button(self, ID_START,label='Start')
        
        self.Bind(wx.EVT_BUTTON, self.on_start_button, id=ID_START)
        
        btn.Add(self.start)
    
        vbox.Add(basics, 0, wx.EXPAND, 0)
        vbox.Add(port, 0, wx.EXPAND, 0)
        vbox.Add(cam, 0, wx.EXPAND, 0)
        vbox.Add(Motor, 0, wx.EXPAND, 0)
        vbox.Add(btn, flag=wx.ALIGN_RIGHT|wx.RIGHT|wx.TOP, border=1)
        
        movie = wx.StaticBoxSizer(wx.StaticBox(self, -1, "Camera"), wx.VERTICAL)
        
        self.panel = wx.Panel(self, wx.NewId(), size=(544,295 ))
        
        movie.Add(self.panel,0, wx.EXPAND, 0)
        
        box.Add(vbox,flag=wx.ALL, border=5)
        box.Add(movie,0, wx.GROW|wx.ALIGN_CENTER_HORIZONTAL|wx.ALL, 5)
    
        

        sizer = wx.BoxSizer(wx.VERTICAL)
        sizer.Add(box, 0, wx.EXPAND,0)
        
        drawbox = wx.StaticBoxSizer(wx.StaticBox(self, -1, "Graph"),wx.VERTICAL)
        drawbox.Add(self.canvas, 1, wx.GROW|wx.ALIGN_CENTER_HORIZONTAL|wx.ALL,0)
        
        sizer.Add(drawbox, 0, wx.EXPAND|wx.ALL,5)
        self.OncomboLO(0)    
        EVT_RESULT(self,self.OnResult)        
        self.SetSizer(sizer)
    
    def init_plot(self):
        
        self.dpi = 100
        self.fig = Figure((8.0, 3.0), dpi=self.dpi)
    
        self.axes1 = self.fig.add_subplot(121)
        self.axes1.set_axis_bgcolor('black')
        self.axes1.set_title('Load Cell 1', size=12)
        
        self.axes2 = self.fig.add_subplot(122)
        self.axes2.set_axis_bgcolor('black')
        self.axes2.set_title('Load Cell 2', size=12)
        
        plt.setp(self.axes1.get_xticklabels(), fontsize=8)
        plt.setp(self.axes1.get_yticklabels(), fontsize=8)
        
        plt.setp(self.axes2.get_xticklabels(), fontsize=8)
        plt.setp(self.axes2.get_yticklabels(), fontsize=8)

        self.plot_data1 = self.axes1.plot(
            self.data1, 
            linewidth=1,
            color=(1, 1, 0),
            )[0]
            
        self.plot_data2 = self.axes2.plot(
            self.data2, 
            linewidth=1,
            color=(1, 1, 0),
            )[0]
        
        #self.draw()
           
    def on_start_button(self,event):
        #
        if not self.worker:
            
            dlg = wx.DirDialog(self,"Choose a directory:", style=wx.DD_DEFAULT_STYLE | wx.DD_NEW_DIR_BUTTON)
            if dlg.ShowModal() == wx.ID_OK:  
                #self.SetStatusText('You selected: %s\n' % dlg.GetPath())
                #f = open(dlg.GetPath()+'\\khar.txt')
                path = dlg.GetPath()
            else:
                self.Result()
                return     
            dlg.Destroy()
            
            
            if self.comboLO.GetValue()=='Static':
                sp1 = self.speed.GetValue()
                spd = float(sp1)
                st1 = self.step.GetValue()
                dur = float(st1)/spd
                sp1 = int(1/(0.1013245*float(sp1)))
                st1 = int(float(st1)*1000/1.51)
                
                st = string.zfill(str(st1), 5)
                sp = ' '+str(sp1)
                cb = 0
                if self.cb.GetValue()==True:
                    cb = float(self.cbctrl.GetValue()) 
                self.worker = WorkerThread(self,self.up1,self.up2,self.up3,self.up4,st,sp,spd,dur,path,cb,self.cr.GetValue,False)
            else:
                sp1 = 2*int(self.fret.GetValue())*float(self.step.GetValue())
                spd = float(sp1)
                st1 = self.step.GetValue()
                dur = 1/int(self.fret.GetValue())/2 
                sp1 = int(1/(0.1013245*float(sp1)))
                st1 = int(float(st1)*1000/1.51)
                
                st = string.zfill(str(st1), 5)
                sp = ' '+str(sp1)
                cb = 0
                if self.cb.GetValue()==True:
                    cb = float(self.cbctrl.GetValue()) 
                self.worker = WorkerThread(self,self.up1,self.up2,self.up3,self.up4,st,sp,spd,dur,path,cb,self.cr.GetValue,[float(self.fret.GetValue()),self.ncycle.GetValue()])
                

            self.Disable()
        else:
            self.worker.abort()
            self.Result()
        
    def OnResult(self, event):
        if event.data==0:
            self.Result()
        else:
            self.paint()
            self.draw()
            
    def OncomboLO(self,event):
        a = self.comboLO.GetValue()
        if a=='Static':
            self.fret.Disable()
            self.fre.Disable()
            self.speedt.Enable()
            self.speed.Enable()
            self.ncycle.Disable()
            self.ncyclestatic.Disable()
        elif a=='Cyclic':
            self.fret.Enable()
            self.fre.Enable()
            self.speedt.Disable()
            self.speed.Disable()
            self.ncycle.Enable()
            self.ncyclestatic.Enable()
            
            
    
    def Result(self):
        self.Enable()
        self.worker = None
        
    def paint(self):
        try:
            image = self.pil_to_image(self.worker.img)
            bmp = wx.BitmapFromImage(image)
            dc = wx.ClientDC(self.panel)
            dc.DrawBitmap(bmp, 0, 0, True)
        except:
            pass
        
    def draw(self):
        try:
            self.data1.append(self.worker.L1)
            self.data2.append(self.worker.L2)

            xmax = len(self.data1) if len(self.data1) > 50 else 50
            xmin = xmax - 100
            #xmin = 0
    
            ymin1 = round(min(self.data1), 0) - 1 
            ymin2 = round(min(self.data2), 0) - 1 
            
            ymax1 = round(max(self.data1), 0) + 1 
            ymax2 = round(max(self.data2), 0) + 1 
            
            self.axes1.set_xbound(lower=xmin, upper=xmax)
            self.axes1.set_ybound(lower=ymin1, upper=ymax1) 
            
            self.axes2.set_xbound(lower=xmin, upper=xmax)
            self.axes2.set_ybound(lower=ymin2, upper=ymax2)
            
            self.axes1.grid(True, color='gray')
            self.axes2.grid(True, color='gray')
        
            self.plot_data1.set_xdata(np.arange(len(self.data1)))
            self.plot_data1.set_ydata(np.array(self.data1))
            self.plot_data2.set_xdata(np.arange(len(self.data2)))
            self.plot_data2.set_ydata(np.array(self.data2))
    
            self.canvas.draw()
        except:
            pass
    
    def OnCamera(self, event):
        c = self.cc.GetValue()
        if c=='one':
            cmr = 0
        else:
            cmr = 1

        
    def OnCbCom(self, event):
        p = self.combopn.GetValue()
        p = p.split(' ')
        portnumber = int(p[1])
        
    
    def OnCbFr(self, event):
        f = self.combopf.GetValue()
        f = f.split(' ')
        fr = float(f[0])
        interval = 1/fr

    
    def OnCbM1(self, event):
        M1 = self.cm1.GetValue()
        if M1=='up':
            self.up1 = 1
        elif M1=='down':
            self.up1 = 2
    
    def OnCbM2(self, event):
        M2 = self.cm2.GetValue()
        if M2=='up':
            self.up2 = 1
        elif M2=='down':
            self.up2 = 2

    
    def OnCbM3(self, event):
        M3 = self.cm3.GetValue()
        if M3=='up':
            self.up3 = 1
        elif M3=='down':
            self.up3 = 2

    
    def OnCbM4(self, event):
        M4 = self.cm4.GetValue()
        if M4=='up':
            self.up4 = 1
        elif M4=='down':
            self.up4 = 2
    
    def Disable(self):
    
            self.speed.Disable()
            self.step.Disable()
            #self.combopf.Disable()
            self.combopn.Disable()
            self.cc.Disable()
            self.cm1.Disable()
            self.cm2.Disable()
            self.cm3.Disable()
            self.cm4.Disable()
            self.cb.Disable()
            self.cbctrl.Disable()
            self.start.SetLabel('Stop') 
    
    def Enable(self):
            
            self.speed.Enable()
            self.step.Enable()
            #self.combopf.Enable()
            self.combopn.Enable()
            self.cc.Enable()
            self.cm1.Enable()
            self.cm2.Enable()
            self.cm3.Enable()
            self.cm4.Enable()
            self.cb.Enable()
            self.cbctrl.Enable()
            self.start.SetLabel('Start') 

    def pil_to_image(self, pil, alpha=True):
        if alpha:
            image = apply( wx.EmptyImage, pil.size )
            image.SetData( pil.convert( "RGB").tostring() )
            image.SetAlphaData(pil.convert("RGBA").tostring()[3::4])
        else:
            image = wx.EmptyImage(pil.size[0], pil.size[1])
            new_image = pil.convert('RGB')
            data = new_image.tostring()
            image.SetData(data)
        return image


    def image_to_pil(self, image):
        pil = Image.new('RGB', (image.GetWidth(), image.GetHeight()))
        pil.fromstring(image.GetData())
        return pil
    
    def OnQuit(self, event):
        self.Close()
        
    def OnOpt(self, event):
        dlg = wx.FileDialog(self,"Choose a file:", style=wx.FD_OPEN)
        if dlg.ShowModal() == wx.ID_OK:
            path = dlg.GetPath()
            wx.BeginBusyCursor()
            data = opt(path)
            data.c = fmin_cobyla(data.optimizec,data.c0,[data.constr,data.constr1,data.constr2],iprint=0,maxfun=10000000,rhoend=1e-8)
            wx.EndBusyCursor()
            dlg = wx.FileDialog(self,"Save file:", style=wx.FD_SAVE)
            if dlg.ShowModal() == wx.ID_OK:
                path = dlg.GetPath()
                data.run(path)
                
    def onCheck(self,event):
        if self.cb.GetValue()==False:
            self.cbctrl.Disable()
        else:
            self.cbctrl.Enable()
EVT_RESULT_ID = wx.NewId()

def EVT_RESULT(win, func):
    """Define Result Event."""
    win.Connect(-1, -1, EVT_RESULT_ID, func)

class ResultEvent(wx.PyEvent):
    """Simple event to carry arbitrary result data."""
    def __init__(self,data):
        """Init Result Event."""
        wx.PyEvent.__init__(self)
        self.SetEventType(EVT_RESULT_ID)
        self.data = data
        
class WorkerThread(Thread):
    """Worker Thread Class."""
    def __init__(self, notify_window, up1, up2, up3, up4, st, sp, spd, dur,dlg,cb,re,isCyclic):
        """Init Worker Thread Class."""
        Thread.__init__(self)
        self._notify_window = notify_window
        self._want_abort = 0

        self.L2 = 0
        self.L1 = 0
        self.isCyclic = isCyclic
        global img
        global cmr
        self.pic = 0
        self.ser = 0
        self.camera = 0
        self.data = info
        self.L1_off = np.zeros(20)
        self.L2_off = np.zeros(20)
        self.L1off = 0
        self.L2off = 0
        
        self.L1_SLope = 0.0515
        self.L2_SLope = 0.0721
        
        self.img = 0
        self.dur = dur
        
        self.up1 = up1
        self.up2 = up2
        self.up3 = up3
        self.up4 = up4
        self.st = st
        self.sp = sp
        self.spd = spd
        self.dlg = dlg
        self.cb = cb
        self.rel = re 
        
        # This starts the thread running on creation, but you could
        # also make the GUI thread responsible for calling this
        self.start()

    def run(self):
        """Run Worker Thread."""
        

        try:
            self.ser = serial.Serial('COM1',
                                baudrate=38400, 
                                bytesize=8, 
                                parity='N', 
                                stopbits=1, 
                                writeTimeout=0.200,
                                timeout = 0.300,
                                interCharTimeout=None)
            self.ser.setDTR(level=False)
            self.ser.setRTS(level=False)
            self.ser.timeout = 0.300
            self.ser.flush()
        except:
            wx.MessageBox('A problem in reading from port', 'error', wx.OK | wx.ICON_ERROR)
            wx.PostEvent(self._notify_window, ResultEvent(0))
            return
        
        try:
            camera = Device(devnum=1)
        except:
            wx.MessageBox('A problem in reading from camera', 'error', wx.OK | wx.ICON_ERROR)
            wx.PostEvent(self._notify_window, ResultEvent(0))
            return
        
        if self.cb!=0:

            self.ser.writelines('P1'+'\r')
            self.ser.writelines('P2'+'\r')
            self.ser.writelines('P3'+'\r')
            self.ser.writelines('P4'+'\r')
            
            for i in range(20):
                self.ser.writelines('L1'+'\r')
                a1 = self.ser.readline()
                a1 = float(a1)
                self.L1_off[i] = a1
                self.ser.writelines('L2'+'\r')
                a2 = self.ser.readline()
                a2 = float(a2)
                self.L2_off[i] = a2
        
            self.L1off = np.average(self.L1_off)
            self.L2off = np.average(self.L2_off)
            
            self.ser.writelines('S'+self.sp+'\r') 
            char = 'M2D'+self.st+'\r'
            self.ser.writelines(char)                            
            char = 'M3D'+self.st+'\r'
            self.ser.writelines(char)
            
            p1 = 0
            while p1<self.cb:
                char = 'L2'+'\r'
                self.ser.writelines(char)
                a1 = self.ser.readline()
                a1 = float(a1)
                p1 = ((a1-self.L2off)*9.8*self.L2_SLope/1000)
                
            self.ser.writelines('P1'+'\r')
            self.ser.writelines('P2'+'\r')
            self.ser.writelines('P3'+'\r')
            self.ser.writelines('P4'+'\r')

            self.ser.writelines('S'+self.sp+'\r') 
            char = 'M1D'+self.st+'\r'
            self.ser.writelines(char)                            
            char = 'M4D'+self.st+'\r'
            self.ser.writelines(char)
            
            p2 = 0
            while p2<self.cb:
                char = 'L1'+'\r'
                self.ser.writelines(char)
                a2 = self.ser.readline()
                a2 = float(a2)
                p2 = ((a2-self.L1off)*9.8*self.L1_SLope/1000) 
                
            self.ser.writelines('P1'+'\r')
            self.ser.writelines('P2'+'\r')
            self.ser.writelines('P3'+'\r')
            self.ser.writelines('P4'+'\r')        



        #offset
        
        
        for i in range(20):
            self.ser.writelines('L1'+'\r')
            a1 = self.ser.readline()
            a1 = float(a1)
            self.L1_off[i] = a1
            self.ser.writelines('L2'+'\r')
            a2 = self.ser.readline()
            a2 = float(a2)
            self.L2_off[i] = a2
        
        self.L1off = np.average(self.L1_off)
        self.L2off = np.average(self.L2_off)
        
        #offset

        
        i = 0

        if self.isCyclic is False:      
            if self.up1+self.up2+self.up3+self.up4>0:
                f = open(self.dlg+'\\data.txt','w')
                self.ser.writelines('S'+self.sp+'\r')       
                if self.up1==1:
                    char = 'M1U'+self.st+'\r'
                    self.ser.writelines(char)
                elif self.up1==2:
                    char = 'M1D'+self.st+'\r'
                    self.ser.writelines(char)
    
                if self.up2==1:
                    char = 'M2U'+self.st+'\r'
                    self.ser.writelines(char)
                elif self.up2==2:
                    char = 'M2D'+self.st+'\r'
                    self.ser.writelines(char)
                                
                if self.up3==1:
                    char = 'M3U'+self.st+'\r'
                    self.ser.writelines(char)
                elif self.up3==2:
                    char = 'M3D'+self.st+'\r'
                    self.ser.writelines(char)
                                
                if self.up4==1:
                    char = 'M4U'+self.st+'\r'
                    self.ser.writelines(char)
                elif self.up4==2:
                    char = 'M4D'+self.st+'\r'
                    self.ser.write(char)
                
                start = time.time()
                t = 0
                while t<self.dur or self.rel:
                        if self._want_abort:
                            wx.PostEvent(self._notify_window, ResultEvent(0))
                            if self.ser._isOpen:
                                self.ser.writelines('P1'+'\r')
                                self.ser.writelines('P2'+'\r')
                                self.ser.writelines('P3'+'\r')
                                self.ser.writelines('P4'+'\r')
                                self.ser.close()
                            return
                        try:
                            char = 'L1'+'\r'
                            self.ser.writelines(char)
                            a1 = self.ser.readline()
                            start1 = time.time()-start
                            a1 = float(a1)
                            
                            char = 'L2'+'\r'
                            self.ser.writelines(char)
                            a2 = self.ser.readline()
                            start2 = time.time()-start
                            a2 = float(a2)
    
                                        
                            self.L1 = ((a1-self.L1off)*9.8*self.L1_SLope/1000)
                            self.L2 = ((a2-self.L2off)*9.8*self.L2_SLope/1000)
    
                            
                            if self.L1>14.7 or self.L2>14.7:
                                wx.PostEvent(self._notify_window, ResultEvent(0))
                                self.abort()
                                return
                            
                            if self.rel and t>=self.dur:
                                f.writelines([str(self.spd*2*(self.dur)),'\t',str(self.L1),'\t\t',str(self.spd*2*self.dur),'\t',str(self.L2)
                                              ,'\t\t',string.zfill(str(i), 5),'\r\n'])
                            else:
                                f.writelines([str(self.spd*2*start1),'\t',str(self.L1),'\t\t',str(self.spd*2*start2),'\t',str(self.L2)
                                              ,'\t\t',string.zfill(str(i), 5),'\r\n'])
                            
                            #camera.saveSnapshot(self.data[0]+'\\'+string.zfill(str(i), 5)+'.jpg')
                            self.img = camera.getImage()
                            self.img.save(self.dlg+'\\'+string.zfill(str(i), 5)+'.jpg')
    
                        except:
                            pass
                        wx.PostEvent(self._notify_window, ResultEvent(1))
                        
                        i += 1
                        t = time.time()-start
            else:
                if self.up1==1:
                    self.up1 = 2
                elif self.up1==2:
                    self.up1 = 1
    
                if self.up2==1:
                    self.up2 = 2
                elif self.up2==2:
                    self.up2 = 1

                if self.up3==1:
                    self.up3 = 2
                elif self.up3==2:
                    self.up3 = 1

                if self.up4==1:
                    self.up4 = 2
                elif self.up4==2:
                    self.up4 = 1
                    
                for ncycle in range(2*self.isCyclic[1]):
                    if self.up1+self.up2+self.up3+self.up4>0:
                        f = open(self.dlg+'\\data.txt','w')
                        self.ser.writelines('S'+self.sp+'\r')       
                        if self.up1==2:
                            char = 'M1U'+self.st+'\r'
                            self.ser.writelines(char)
                            self.up1 = 1
                        elif self.up1==1:
                            char = 'M1D'+self.st+'\r'
                            self.ser.writelines(char)
                            self.up1 = 2
            
                        if self.up2==2:
                            char = 'M2U'+self.st+'\r'
                            self.ser.writelines(char)
                            self.up2 = 1
                        elif self.up2==1:
                            char = 'M2D'+self.st+'\r'
                            self.ser.writelines(char)
                            self.up2 = 2
                                        
                        if self.up3==2:
                            char = 'M3U'+self.st+'\r'
                            self.ser.writelines(char)
                            self.up3 = 1
                        elif self.up3==1:
                            char = 'M3D'+self.st+'\r'
                            self.ser.writelines(char)
                            self.up3 = 2
                                        
                        if self.up4==2:
                            char = 'M4U'+self.st+'\r'
                            self.ser.writelines(char)
                            self.up4 = 1
                        elif self.up4==1:
                            char = 'M4D'+self.st+'\r'
                            self.ser.write(char)
                            self.up4 = 2
                        
                        start = time.time()
                        t = 0
                        
                        while t<self.dur or self.rel:
                                if self._want_abort:
                                    wx.PostEvent(self._notify_window, ResultEvent(0))
                                    if self.ser._isOpen:
                                        self.ser.writelines('P1'+'\r')
                                        self.ser.writelines('P2'+'\r')
                                        self.ser.writelines('P3'+'\r')
                                        self.ser.writelines('P4'+'\r')
                                        self.ser.close()
                                    return
                                try:
                                    char = 'L1'+'\r'
                                    self.ser.writelines(char)
                                    a1 = self.ser.readline()
                                    start1 = time.time()-start
                                    a1 = float(a1)
                                    
                                    char = 'L2'+'\r'
                                    self.ser.writelines(char)
                                    a2 = self.ser.readline()
                                    start2 = time.time()-start
                                    a2 = float(a2)
            
                                                
                                    self.L1 = ((a1-self.L1off)*9.8*self.L1_SLope/1000)
                                    self.L2 = ((a2-self.L2off)*9.8*self.L2_SLope/1000)
            
                                    
                                    if self.L1>14.7 or self.L2>14.7:
                                        wx.PostEvent(self._notify_window, ResultEvent(0))
                                        self.abort()
                                        return
                                    
                                    if self.rel and t>=self.dur:
                                        f.writelines([str(self.spd*2*(self.dur)),'\t',str(self.L1),'\t\t',str(self.spd*2*self.dur),'\t',str(self.L2)
                                                      ,'\t\t',string.zfill(str(i), 5),'\r\n'])
                                    else:
                                        f.writelines([str(self.spd*2*start1),'\t',str(self.L1),'\t\t',str(self.spd*2*start2),'\t',str(self.L2)
                                                      ,'\t\t',string.zfill(str(i), 5),'\r\n'])
                                    
                                    #camera.saveSnapshot(self.data[0]+'\\'+string.zfill(str(i), 5)+'.jpg')
                                    self.img = camera.getImage()
                                    self.img.save(self.dlg+'\\'+string.zfill(str(i), 5)+'.jpg')
            
                                except:
                                    pass
                                wx.PostEvent(self._notify_window, ResultEvent(1))
                                
                                i += 1
                                t = time.time()-start



        wx.PostEvent(self._notify_window, ResultEvent(0))
        
    wx.EVT_HOTKEY
    def abort(self):
        self._want_abort = 1
        
        

if __name__ == '__main__':
        app = wx.App()
        MotorControl(None, title="Motor Control")
        app.MainLoop()