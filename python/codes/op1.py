from scipy.optimize import fmin_cobyla
import numpy as np

class opt():
    def __init__(self,path):
        
        self.f = open(path,'r')
        self.l1 = 0
        self.l2 = 0
        self.t = 0
        self.c = []
        
        idp1 = None
        idp2 = None
        iddis1 = None
        iddis2 = None
        self.biaxial = False
        dis1 = []
        dis2 = []
        p1 = []
        p2 = []
        self.c0 = [1,1,1,1,1]
        cnt = 0
        flag = 0
        readline = self.f.readlines()
        while cnt<len(readline):
            line = readline[cnt]
            cnt = cnt+1
            if len(line)<8:
                continue
            val = line.split()
            count = 0
            for item in val:
                if item=='biaxial':
                    self.biaxial = True
                elif item=='uniaxial':
                    self.biaxial = False
                if item=='c0':
                    self.c0 = map(float, val[count+1:count+6])
                if (self.l1==0 or self.l2==0 or self.t==0):
                    it = item.split('=')
                    if it[0]=='l1':
                        self.l2 = np.float(it[1])
                    elif it[0]=='l2':
                        self.l1 = np.float(it[1])
                    elif it[0]=='t':
                        self.t = np.float(it[1])
                
                if (iddis1==None or iddis2==None or idp1==None or idp2==None):
                    if item=='dis1':
                        iddis2 = count
                    if item=='dis2':
                        iddis1 = count
                    if item=='p1':
                        idp2 = count
                    if item=='p2':
                        idp1 = count
                count = count+1
            if not(iddis1==None or iddis2==None or idp1==None or idp2==None) and flag==0:
                flag = 1
                continue
            if iddis1!=None:
                dis1.append(np.float(val[iddis1]))
            if iddis2!=None:
                dis2.append(np.float(val[iddis2]))
            if idp1!=None:
                p1.append(np.float(val[idp1]))
            if idp2!=None:
                p2.append(np.float(val[idp2]))     
        
        dis1 = np.array(dis1)
        dis2 = np.array(dis2)
        p1 = np.abs(np.array(p1))
        p2 = np.abs(np.array(p2))

        self.landa1 = (self.l1+dis1)/self.l1
        if (dis2==None):
            dis2 = dis1
            p2 = p1
            self.biaxial = False
        self.landa2 = (self.l2+dis2)/self.l2
        self.sigma_exp1 = self.landa1*(p1/(self.l2*self.t))
        self.sigma_exp2 = self.landa2*(p2/(self.l1*self.t))
        self.sigma_model1 = np.zeros(len(dis1))
        self.sigma_model2 = self.sigma_model1
        self.landa32 = np.power(self.landa1,-2)*np.power(self.landa2,-2)
        self.landa12 = np.power(self.landa1,2)
        self.landa22 = np.power(self.landa2,2)
        

    def optimizec(self,c):
    
        self.sigma_model1 = (2*self.landa1*c[0]*(self.landa1-1)+3*self.landa1*c[1]*np.power((self.landa1-1),2)
                             +self.landa1*c[3]*(self.landa12+self.landa22+self.landa32-3)+2*(self.landa12-self.landa32)*(c[2]+c[3]*
                                                            (self.landa1-1))+2*c[4]*(self.landa12+self.landa22+self.landa32-3))
        if (self.biaxial):
            self.sigma_model2 = (2*(self.landa22-self.landa32)*(c[2]+c[3]*(self.landa1-1)+2*c[4]*(self.landa12+self.landa22+self.landa32-3)))
            #a = sum(np.power((self.sigma_exp1-self.sigma_model1),2)+np.power((self.sigma_exp2-self.sigma_model2),2))
            #print a
            return sum(np.power((100*self.sigma_exp1-100*self.sigma_model1),2)+np.power((100*self.sigma_exp2-100*self.sigma_model2),2))
        else:
            return sum(np.power((self.sigma_exp1-self.sigma_model1),2))
        
    def constr(self,c):
        if (np.count_nonzero((3*c[1]-2*c[0]-3*c[3]+(2*c[0]-6*c[1])*self.landa1+(3*c[1]+2*c[3])*
                              np.power(self.landa1,2)+c[3]/np.power(self.landa1,4))>0)==len(self.landa1)):
            return 1
        else:
            return -1
    
    def constr1(self,c):
        if (self.sigma_model1<0).any():
            return -1
        else:
            return 1
    
    def constr2(self,c):
        if (self.sigma_model2<0).any():
            return -1
        else:
            return 1
    
    def run(self,path1):
        self.sigma_model1 = (2*self.landa1*self.c[0]*(self.landa1-1)+3*self.landa1*self.c[1]*
                             np.power((self.landa1-1),2)+self.landa1*self.c[3]*(self.landa12+
                            self.landa22+self.landa32-3)+2*(self.landa12-self.landa32)*
                             (self.c[2]+self.c[3]*(self.landa1-1))+2*self.c[4]*(self.landa12+
                                                                self.landa22+self.landa32-3))
        if (self.biaxial):
            self.sigma_model2 = (2*(self.landa22-self.landa32)*(self.c[2]+self.c[3]*(self.landa1-1)+
                                            2*self.c[4]*(self.landa12+self.landa22+self.landa32-3)))
        f1 = open(path1,'w')
        f1.writelines(['c0','\t',str(self.c0[0]),'\t',str(self.c0[1]),'\t',str(self.c0[2]),'\t',
                           str(self.c0[3]),'\t',str(self.c0[4]),'\n'])
        f1.writelines(['c','\t',str(self.c[0]),'\t',str(self.c[1]),'\t',str(self.c[2]),'\t',
                           str(self.c[3]),'\t',str(self.c[4]),'\n'])
        f1.writelines(['l1='+str(self.l1),'\t','l2='+str(self.l2),'\t','t='+str(self.t),'\n'])
        if self.biaxial:
            f1.writelines(['bioaxial','\n'])
        else:
            f1.writelines(['uniaxial','\n'])
            
        f1.writelines(['sigma_exp2','\t','sigma_model2','\t','elong2','\t\t','sigma_exp1',
                           '\t','sigma_model1','\t','elong1','\n'])
        for i in range(len(self.landa1)):
            f1.writelines([str(self.sigma_exp1[i]),'\t',str(self.sigma_model1[i]),'\t',str(self.landa1[i]),'\t\t',
                               str(self.sigma_exp2[i]),'\t',str(self.sigma_model2[i]),'\t',str(self.landa2[i]),'\n'])
    
#x = 85
#y = 90

#print np.log10(y-5)**5/2**6*np.log10(x)*x


