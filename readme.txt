
# Setting Up Environment for the Python 2.7 Code

To run the program, you need to set up the required dependencies and create a suitable environment. Here's how you can do it step by step:

## Step 1: Install Python 2.7
1. Download Python 2.7 installer for Windows from the official website: [Python 2.7 Download](https://www.python.org/downloads/release/python-278/).
2. Run the installer and follow the installation instructions. Make sure to add Python to your system's PATH during installation.

## Step 2: Install Required Packages
Open a command prompt (CMD) and execute the following commands to install the required packages:

```bash
pip install wxPython
pip install pyserial
pip install numpy
pip install scipy
pip install opencv-python
```

## Step 3: Install VideoCapture Package

To make use of VideoCapture just copy the files from the 'PythonXX' folder to the corresponding folders of your 'PythonXX' installation, where XX must match with the version of Python you have installed on your system.

README.md
