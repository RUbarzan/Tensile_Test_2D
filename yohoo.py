import serial ## serial port controller
import time ## The library to handle time-related jobs
import string ## Common String Operations



ser = serial.Serial('COM4',
                         baudrate=38400, 
                         bytesize=8, 
                         parity='N', 
                         stopbits=1, 
                         writeTimeout=0.200,
                         timeout = 0.300,
                         interCharTimeout=None)
ser.setDTR(level=False)
ser.setRTS(level=False)
ser.timeout = 0.300
ser.flush()

st = '05000'
char = 'M1U'+st+'\r'
 
for i in range(1,1):
    ser.writelines(char)